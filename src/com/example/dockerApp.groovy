#!/usr/bin/env groovy

package com.example

class dockerApp implements Serializable {
    def script 


    dockerApp(script) {
        this.script = script
    }

    def buildDockerImage (String ImageName){
    script.echo "Building the Application in server 3.5 ...."

        script.sh "docker build . -t $ImageName"
        
        
        
    }

    def dockerLogin() {
        script.withCredentials([script.usernamePassword(credentialsId: "Dockerhub", usernameVariable: "USER", passwordVariable:"PASS")]) {
            script.sh "echo $script.PASS | docker login -u $script.USER --password-stdin"

        }

    

    }

    def dockerPush(String ImageName) {
        script.sh "docker push $ImageName"
    }

}